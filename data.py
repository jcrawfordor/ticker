from peewee import *
db = SqliteDatabase('lists.db')

class BaseModel(Model):
    class Meta:
        database = db

class List(BaseModel):
    name = TextField()

class Category(BaseModel):
    name = TextField()
    order = IntegerField(default=0)
    list = ForeignKeyField(List, backref='categories', on_delete='CASCADE')

class Item(BaseModel):
    category = ForeignKeyField(Category, backref='items', on_delete='CASCADE')
    name = TextField()
