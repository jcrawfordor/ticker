from flask import Flask, render_template, request
from escpos.printer import Serial
from data import List, Item, db, Category
from peewee import fn
from datetime import datetime

app = Flask(__name__)
db.connect()
db.create_tables([List, Item, Category])


def build_index(flash=None):
    lists = List.select()
    return render_template('index.html', lists=lists, flash=flash)


def print_list(list):
    p = Serial(devfile="/dev/ttyUSB0", baudrate=38400, bytesize=8, parity='N', stopbits=1, timeout=1.00, dsrdtr=True)
    p.set('center', 'A', 'B', 2, 2)
    p.text(f"{list.name}\n")
    for category in list.categories:
        p.set('center', 'B', 'normal', 2, 2)
        p.text(f"\n{category.name}\n")
        p.set('left', 'A', 'normal', 1, 1)
        for item in category.items:
            p.text(f"[  ] {item.name}\n")
    date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    p.set('right', 'B', 'normal', 1, 1)
    p.text(f"\nas of {date}\n")
    p.cut()


@app.route('/', methods=['GET', 'POST'])
def default_ui():
    if 'object' in request.form:
        if request.form['object'] == "item":
            return mod_item(request)
        elif request.form['object'] == "list":
            return mod_list(request)
        elif request.form['object'] == "category":
            return mod_category(request)
    else:
        return build_index()


def mod_item(request):
    list_name = request.form['list']
    item_name = request.form['item']
    category_name = request.form['category']
    action = request.form['action']
    if action == "add":
        list = List.get(List.name == list_name)
        category = Category.get(Category.list == list, Category.name == category_name)
        item = Item.get_or_create(name=item_name, category=category)
        flash = "Added item"
    if action == "del":
        list = List.get(List.name == list_name)
        category = Category.get(Category.list == list, Category.name == category_name)
        item = Item.get(Item.name == item_name, Item.category == category)
        item.delete_instance()
        flash = "Deleted item"
    return build_index(flash)


def mod_list(request):
    list_name = request.form['list']
    action = request.form['action']
    if action == "add":
        list = List.get_or_create(name=list_name)
        flash = "List added"
    if action == "delete":
        if 'confirm' in request.form and request.form['confirm'] == 'true':
            list = List.get(List.name == list_name)
            list.delete_instance(recursive=True)
            flash = "List deleted"
        else:
            return render_template('confirm.html', object="list", action=action, list=list_name)
    if action == "clear":
        if 'confirm' in request.form and request.form['confirm'] == 'true':
            list = List.get(List.name == list_name)
            for category in list.categories:
                query = Item.delete().where(Item.category == category).execute()
            flash = "List cleared"
        else:
            return render_template('confirm.html', object="list", action=action, list=list_name)
    if action == "print":
        list = List.get(List.name == list_name)
        print_list(list)
        flash = "Printed list"
    if action == "cancel":
        flash = "Cancelled"
    if action == "add category":
        list = List.get(List.name == list_name)
        category_name = request.form['category']
        if Category.select().where(Category.list == list).count() > 0:
            this_order = Category.select(fn.MAX(Category.order)).where(Category.list == list).scalar() + 1
        else:
            this_order = 0
        category = Category.get_or_create(list=list, name=category_name, order=this_order)
        flash = "Category added"
    return build_index(flash)


def mod_category(request):
    list_name = request.form['list']
    category_name = request.form['category']
    action = request.form['action']
    list_obj = List.get(List.name == list_name)
    if action == "del":
        if 'confirm' in request.form and request.form['confirm'] == 'true':
            category = Category.get(Category.list == list_obj, Category.name == category_name)
            category.delete_instance(recursive=True)
            flash = "Category deleted"
        else:
            return render_template('confirm.html', object="category", action=action, list=list_name, category=category_name)
    if action == "up":
        current_order = Category.get(Category.list == list_obj, Category.name == category_name).order
        if current_order != 0:
            this = Category.get(Category.list == list_obj, Category.order == current_order)
            prev = Category.get(Category.list == list_obj, Category.order == current_order - 1)
            this.order -= 1
            this.save()
            prev.order += 1
            prev.save()
            flash = "Category moved"
        else:
            flash = "Already at top"
    if action == "down":
        current_order = Category.get(Category.list == list_obj, Category.name == category_name).order
        max_order = Category.select(fn.MAX(Category.order)).where(Category.list == list_obj).scalar()
        if current_order != max_order:
            this = Category.get(Category.list == list_obj, Category.order == current_order)
            next = Category.get(Category.list == list_obj, Category.order == current_order + 1)
            this.order += 1
            this.save()
            next.order -= 1
            next.save()
            flash = "Category moved"
        else:
            flash = "Already at bottom"
    if action == "cancel":
        flash = "Cancelled"
    return build_index(flash)
